## css选择器
* id 选择器 id作为选择
```
#red {color:red;}
#green {color:green;}
```
对应HTML:
```
<p id="red">这个段落是红色。</p>
<p id="green">这个段落是绿色。</p>
```


* class选择器 以class作为选择,css中以 点 号显示
```
.center {text-align: center}
```
对应HTML：
```
<h1 class="center">
This heading will be center-aligned
</h1>

<p class="center">
This paragraph will also be center-aligned.
</p>
```
* CSS 派生选择器
```
strong {
     color: red;
     }

h2 {
     color: red;
     }

h2 strong {
     color: blue;
     }
```
对应的html:
```
<p>The strongly emphasized word in this paragraph is<strong>red</strong>.</p>
<h2>This subhead is also red.</h2>
<h2>The strongly emphasized word in this subhead is<strong>blue</strong>.</h2>
```
* CSS 属性选择器: IE7 和 IE8 才支持属性选择器。在 IE6 及更低的版本中，不支持属性选择
```
[title]
{
color:red;
}
```
## 学习网址:
[css学习网址](http://www.w3school.com.cn/css/css_syntax_descendant_selector.asp).

### 任何元素都要以下三个属性和宽和高，都是一个盒子，也就是盒子模型
```
boder     # 边框大小
margin   # 和其他元素的距离
padding   # 自身大小
```
### css 继承写法
```
/* 继承写法 */
.s_btn,#s_btn_add{
    margin-left: 30px;
}

<button class="s_btn"  id="s_btn_add" type="button"> 增加业务 </button>
```

