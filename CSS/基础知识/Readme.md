##基础语法
h1 是选择器,color和font-size表示属性
```
h1 {color:red; font-size:14px;}
```

##选择器分组
```
h1,h2,h3,h4,h5,h6 {
  color: green;
  }
```

##派生选择器
你希望列表中的strong元素变为斜体字,而不是通常的粗体字,可以这样定义一个派生选择器：
```
li strong {
    font-style: italic;
    font-weight: normal;
  }
```

