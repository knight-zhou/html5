## 当元素包含元素的时候就有边距的说法
### 内边距-padding属性
[练习](http://www.w3school.com.cn/cssref/pr_padding-top.asp)
* padding-top
* padding-bottom
* padding-right
* padding-left
```
// 简写
p{
  padding:2cm 4cm 3cm 4cm;
  }

//分开写
td {padding-right: 5cm}
```
### 边框 `border` 属性
* border-top-style
* border-right-style
* border-left-style
* border-bottom-style
```
 值		描述
none	定义无边框。
hidden	与 "none" 相同。不过应用于表时除外，对于表，hidden 用于解决边框冲突。
dotted	定义点状边框。在大多数浏览器中呈现为实线。
dashed	定义虚线。在大多数浏览器中呈现为实线。
solid	定义实线。
double	定义双线。双线的宽度等于 border-width 的值。
groove	定义 3D 凹槽边框。其效果取决于 border-color 的值。
ridge	定义 3D 垄状边框。其效果取决于 border-color 的值。
inset	定义 3D inset 边框。其效果取决于 border-color 的值。
outset	定义 3D outset 边框。其效果取决于 border-color 的值。
inherit	规定应该从父元素继承边框样式。
```
### 设置上边框宽度
```
p{
  border-style:solid;
  border-top-width:15px;
  }
```

### 外边距 `margin` 属性
* margin-bottom
* margin-top
* margin-left
* margin-right

