## 绝对定位
absolute: 绝对的意思
```
h2{
  position:absolute;
  left:100px;
  top:150px;
  }
或者使用百分比
img{
position:absolute;
top:60%
}
```

### 相对定位
随浏览器的窗口大小而随之变化
```
h2{
	position:relative;
	left:20px
}
``` 

## 使用固定定位
也就是固定元素在某个位置，不随浏览器窗口的变化
```
p{
	position:fixed;
    top:30px;
	right:5px;
```
## 设置滚动条框住元素溢出
overflow属性控制,如果想隐藏溢出的内容,将其值设置为hidden
```
div 
{
background-color:#00FFFF;
width:150px;
height:150px;
overflow: scroll;
}
```
## 剪切图像
```
img {
position:absolute;
clip:rect(0px 100px 200px 0px)
}
```
### css定位属性有如下:
```
属性			描述
position		把元素放置到一个静态的、相对的、绝对的、或固定的位置中。
top				定义了一个定位元素的上外边距边界与其包含块上边界之间的偏移。
right			定义了定位元素右外边距边界与其包含块右边界之间的偏移。
bottom			定义了定位元素下外边距边界与其包含块下边界之间的偏移。
left			定义了定位元素左外边距边界与其包含块左边界之间的偏移。
overflow		设置当元素的内容溢出其区域时发生的事情。
clip			设置元素的形状。元素被剪入这个形状之中，然后显示出来。
vertical-align	设置元素的垂直对齐方式。
z-index			设置元素的堆叠顺序。
```

## 浮动
[浮动学习链接](http://www.w3school.com.cn/css/css_positioning_floating.asp)
```
<html>
<head>
<style type="text/css">
span{
	float:left;
	width:0.7em;
	font-size:400%;
	font-family:algerian,courier;
	line-height:80%;
}
</style>
</head>

<body>
<p>
<span>T</span>his is some text.
This is some text. This is some text.
This is some text. This is some text. This is some text.
</p>
<body>
</html>
```
