## css文本
### 背景色
```
p{
	background-color: gray;
}
```
### 背景图像
```
body {background-image: url(/i/eg_bg_04.gif);}
```
### 缩进文本
这个属性最常见的用途是将段落的首行缩进
```
p {text-indent: 5cm;}
```
### 字间隔
```
p{
  word-spacing:25px;
}
```

详细请参看:
[css文本](http://www.w3school.com.cn/css/css_text.asp)

##表格
text-align 属性设置水平对齐方式，比如左对齐、右对齐或者居中：

vertical-align 属性设置垂直对齐方式，比如顶部对齐、底部对齐或居中对齐:
```
td{
  height:50px;
  vertical-align:bottom;
  }
```

