## DOM
* D: document
* O: object 
* M: Model

## 节点
* 元素节点
* 文本节点
* 属性节点

## DOM方法
`getElementById` 方法
```
<script type="text/javascript">
alert(typeof document.getElementById('hahaaha'))
</script>
```

getElementsByTagName 方法: 返回带有指定标签名的对象的集合
```
var shopping = document.getElementById('hahaaha');
var items = shopping.getElementsByTagName("*");
alert(items.length);
```

getElementsByClassName 方法: 这个方法可以通过class属性中的类名来访问元素
```
var shopping = document.getElementById('hahaaha');
var sales = shopping.getElementsByClassName("sale");
alert(sales.length);
```

getAttribute 方法 : 指定属性名的属性值
```
<script type="text/javascript">
var paras = document.getElementsByTagName("p");
for (var i = 0;i < paras.length; i++ ){
	alert(paras[i].getAttribute("title"));

}
```

setAttribute方法: 对属性节点的值做修改
```
<script type="text/javascript">
var shopping = document.getElementById("tt");
shopping.setAttribute("title","a list of goods");

}
```

