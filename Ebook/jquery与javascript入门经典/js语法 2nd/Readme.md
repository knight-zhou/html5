##数组
数组是指 一个变量表示一个值的集合,集合中的每个值都是一个这个数组的一个元素,例如我们可以用名为galas乐队
的变量来保存GALAS乐队全体四位成员的姓名
## 声明数组
```
var galas = Array(4);    //声明长度
var galas = Array();    // 无法预知数组元素用这个表示
galas[0] = 'Tom';   
galas[1] = 'Knight';   //填充数组

var galas = Array('Tom','knight','xx','oo');   //类似于py中的列表
var galas = ['Tom','knight','xx','oo'];   //简单写法
```
## 对象
js中的对象类似于py中的字典
```
var xxoo = Object();
xxoo.name = 'John';
xxoo.year = 1988;
xxoo.living= True;

//简单写法写对象
var xxoo = {name:"John",year:1988,living:True};

```
## 算术操作符
```
var a = 95;
var b = (a-32)/1.8;

alert("10"+30)   //弹出框
alert(10+30)
```
## 条件语句
if条件句:

```
if (1>2) {

	alert('the world has gone mad!')
}
else {
alert('all is well with the world')

}
```
比较操作符
```
 var a = 'happy';
 var b = 'sad';
 if (a == b){     //不等于用!=

 	alert('we both feel the same')
 }
 else {

 	alert('we are have the diff feel')
 }
```
逻辑操作符
```
var num = 7;
if (num >=5 && num <= 10){  //逻辑与,逻辑或为 ||
// if (!(1>2)){  //逻辑非
	alert('the number is in the right range')

}

```
while 循环
```
var cc = 1;
while (cc < 11) {
	alert(cc)
	cc++;
}

```
for 循环
```
for (var cc = 1;cc < 11;cc++) {
	// alert(cc);
	document.write(cc+'<br>');
}

```
函数
```
function name(agrs) {
	statements;	
}

```
函数举例
```
function xxoo(n1,n2) {
	var total = n1 * n2;
	// return total;
	return total;
}
var num = xxoo(5,6)
alert(num)
```
内建对象
```
var num = 8;
var num = Math.round(num);
alert(num);
```
Date对象
```
var date = new Date()
alert(date)
```




