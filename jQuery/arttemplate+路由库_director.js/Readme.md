### 官方文档
[art-template 官方文档](https://aui.github.io/art-template/zh-cn/docs/index.html)


###arttemplate 解决了渲染的问题，路由问题可以用第三方路由实现
[第三方路由:page.js 或者director](https://cn.vuejs.org/v2/guide/routing.html)

#### vue 的路由底层主要采用HTML5 History API 的原理

## director.js 路由库学习
[网址](https://www.cnblogs.com/mj878/p/5944552.html)

## 什么是js对象
* js 提供多个内建对象，比如 String、Date、Array 等等
* js对象只是带有属性和方法的特殊数据类型。
