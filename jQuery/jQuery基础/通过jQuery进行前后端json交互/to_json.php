<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "test";
 
// 创建连接，面向对象
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("连接失败: " . $conn->connect_error);
} 
 
$arr = array();
$sql = "SELECT id, name FROM student";
$result = $conn->query($sql);
 
while($row = $result->fetch_assoc()) { 
  $count=count($row);//不能在循环语句中，由于每次删除row数组长度都减小 
  for($i=0;$i<$count;$i++){ 
    unset($row[$i]);//删除冗余数据 
  } 
  array_push($arr,$row); 
  
}  
 
echo json_encode($arr,JSON_UNESCAPED_UNICODE);  //json编码 
$conn->close(); 