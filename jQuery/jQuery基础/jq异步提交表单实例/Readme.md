## ajax 我终于会了
请看如下代码:
```
// 改成异步提交方式
$(function(){
    var domain=$("input[name='domain']").val()   // 获取input的值,js对象
    var action=$("input[name='action']").val()   // 获取input的值,js对象
    var op=$("input[name='op']").val()   // 获取input的值,js对象
    var command=$("input[name='command']").val()   // 获取input的值,js对象
    var data1 = $(".myForm").serialize();  // 序列化表单内容

    $(".a_click").click(function(){
            $.ajax({
			//	url:"/oms_platform/poms/index.php/home/Fabu/sync", // 绝对路径以斜杠开头,根目录开始
                url:"http://127.0.0.1/oms_platform/poms/index.php/home/Fabu/sync",
                type:"post",
                data:data1,
                success:function(msg){   // 请求成功的操作,其中msg表示后端的返回数据
                    console.log(msg);    
                    //console.log(data1);   // 前台数据   
                }

            });
        // $(".myForm").submit(); 
        // console.log(data1);            
    });
});
```
success:function(msg) 代表ajax方法返回的数据，也即是参数.如果你ajax 调用的方法返回的是bool类型的值,那msg 就等于true或false,是string 类型的 那msg就是一个数据

也就是等价于 后端返回的数据.
