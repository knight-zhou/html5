## 内置效果
#### 显示 隐藏和切换
* .show()和.hide()函数分别显示和隐藏页面元素
*  更多功能请看 api文档, http://api.jquery.com/category/effects/

#### 滑动
```
.sideDown()函数实现
```

#### 拖和放
```
.draggable()和.droppable() 实现
```

