### ajax并不是全新的技术 而是把多种技术糅合在一起，ajax的主要组成技术如下:
* HTML
* CSS
* DOM (HTML DOM AND XML DOM)
* JavaScript
* XML
* XMLHttpRequest(核心)

## jQuery使ajax的使用更加简单
ajax()函数的基本语法如下:
```
$.ajax({
	parameter: value	
});
```
可以像ajax函数传递若干parameter: value 对,不过通常情况下会指定方法，url和一个回调函数。

很常见的还有指定返回的数据类型是否缓存响应，被传递到服务器的数据以及错误发生时该怎么做

## jQuery 的ajax学习网址
### 学习这个的话 对着 jQuery手册 即学即用
[jQuery的ajax学习网址](http://www.w3school.com.cn/jquery/jquery_ref_ajax.asp)

