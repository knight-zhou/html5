## jQuery Ajax 操作函数
jQuery 库拥有完整的 Ajax 兼容套件。其中的函数和方法允许我们在不刷新浏览器的情况下从服务器加载数据。
```
函数 				描述
jQuery.ajax() 		执行异步 HTTP (Ajax) 请求。
.ajaxComplete() 	当 Ajax 请求完成时注册要调用的处理程序。这是一个 Ajax 事件。
.ajaxError() 		当 Ajax 请求完成且出现错误时注册要调用的处理程序。这是一个 Ajax 事件。
.ajaxSend() 		在 Ajax 请求发送之前显示一条消息。
jQuery.ajaxSetup() 	设置将来的 Ajax 请求的默认值。
.ajaxStart() 		当首个 Ajax 请求完成开始时注册要调用的处理程序。这是一个 Ajax 事件。
.ajaxStop() 		当所有 Ajax 请求完成时注册要调用的处理程序。这是一个 Ajax 事件。
.ajaxSuccess() 		当 Ajax 请求成功完成时显示一条消息。
jQuery.get() 		使用 HTTP GET 请求从服务器加载数据。
jQuery.getJSON() 	使用 HTTP GET 请求从服务器加载 JSON 编码数据。
jQuery.getScript() 	使用 HTTP GET 请求从服务器加载 JavaScript 文件，然后执行该文件。
.load() 			从服务器加载数据，然后把返回到 HTML 放入匹配元素。
jQuery.param() 		创建数组或对象的序列化表示，适合在 URL 查询字符串或 Ajax 请求中使用。
jQuery.post() 		使用 HTTP POST 请求从服务器加载数据。
.serialize() 		将表单内容序列化为字符串。
.serializeArray() 	序列化表单元素，返回 JSON 数据结构数据。
```