## 声明变量
```
JavaScript是弱类型语言，声明变量时用var关键字（注意var要全部小写）就可以了
```
## javascript定义变量时带var与不带var的区别分析
```
<script language="javascript" type="text/javascript">
var abc=89;//带var，表示全局变量
function test(){
 var abc=80;//在函数内部，如果不带var，表示使用函数外全局变量；带上var，表示新定义一个全局变量
}
test();
alert(abc);
</script>

```
### 说明
严格来说：函数体内不带var，并不是指定义一个变量，而是进行变量赋值，即var abc;abc=8。

在函数体内如果进行赋值 abc=80（不带var），实际过程是这样的该语句先在函数体内查找变量abc，如果找不到，它会往上在函数体外继续查找变量abc，如果还是找不到，最后没有办法，只能在函数体外定义变量var abc。

