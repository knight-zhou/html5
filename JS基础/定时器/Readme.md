## 定时器
JavaScript 包含一个timers的函数，它用来定时时间或者一个给定的时间间隔延迟代码的执行.
### javascript 定时器包含两个全局函数
* setTimeout()
* clearTimeout()
* setInterval()
* clearInterval()